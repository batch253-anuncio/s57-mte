let collection = [];

// Write the queue functions below.

function print() {
  return collection;
}

function enqueue(data) {
  collection.push(data);
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}

function front() {
  return collection[0];
}

function size() {
  return collection.length;
}

function isEmpty() {
  return !collection;
}

console.log(isEmpty());

module.exports = {
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
